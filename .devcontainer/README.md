# EDSF - Event Driven Science Fair
This project collects events from various booths and sources during a conference, science fair, meetup, etc. It places them
in a stream of data and builds a real time dashboard around them.

## Resources

https://javascript.plainenglish.io/creating-a-rest-api-with-jwt-authentication-and-role-based-authorization-using-typescript-fbfa3cab22a4