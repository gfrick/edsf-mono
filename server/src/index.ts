import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response} from "express";
import {AppDataSource} from "./data-source";
import {Routes} from "./routes";
import {BoothEvent} from "./entity/BoothEvent";
import {allKeys, allowedMessageTypes, ipMessageTypeBlock} from "./access";
import {getRequestIpAddress} from "./find-ip";

const MAX_IP_BLOCK = 4;

AppDataSource.initialize().then(async () => {

    // create express app
    const app = express();
    app.use(bodyParser.json());

    // register express routes from defined application routes
    Routes.forEach(route => {
        (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            const token = req.headers["authorization"];
            const ip = getRequestIpAddress(req);
            if (!allKeys.includes(token)) {
                res.send(403, "Authorization token required.");
                return;
            }

            if( route.method !== 'get') {

                let event = req.body;
                const allowedTypes = allowedMessageTypes.find((amt) => amt.key === token).types;
                if (!allowedTypes.includes(event.messageType)) {
                    res.send(403, `You can only send messageType(s): ${allowedTypes.join(', ')}`);
                    return;
                }

                const ipBlock = ipMessageTypeBlock.find((amt) => amt.key === token);
                const ipSet = ipBlock.ip;
                if (!ipSet.includes(ip)) {
                    ipSet.push(ip);
                    if (ipSet.length > MAX_IP_BLOCK) {
                        ipBlock.banned = true;
                    }
                }
                if (ipBlock.banned || !ipSet.includes(ip)) {
                    res.send(403, "You may only call this from three different IPs.");
                    return;
                }

                console.log(allowedMessageTypes[token]);
            }
            const result = (new (route.controller as any))[route.action](req, res, next)
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined)
            } else if (result !== null && result !== undefined) {
                res.json(result)
            }
        })
    })

    // setup express app here
    // ...

    // start express server
    app.listen(8080)

    await AppDataSource.manager.save(
        AppDataSource.manager.create(BoothEvent, {
            approved: true,
            message: "First Message",
            messageType: "EDSF",
            received: new Date(),
            json: "{}"
        })
    );

    // insert new users for test
    // await AppDataSource.manager.save(
    //     AppDataSource.manager.create(User, {
    //         firstName: "Timber",
    //         lastName: "Saw",
    //         age: 27
    //     })
    // )

    console.log("Express server has started on port 8080. Open http://localhost:8080/boothEvents to see results")

}).catch(error => console.log(error))
