import "reflect-metadata"
import { DataSource } from "typeorm"
import { User } from "./entity/User"
import { BoothEvent } from "./entity/BoothEvent"

export const AppDataSource = new DataSource({
    type: "sqlite",
    database: "database.sqlite",
    synchronize: true,
    logging: false,
    entities: [User, BoothEvent],
    migrations: [],
    subscribers: [],
})
