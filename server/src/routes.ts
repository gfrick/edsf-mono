import { UserController } from "./controller/UserController";
import { BoothEventController } from "./controller/BoothEventController";

export const Routes = [{
    method: "get",
    route: "/users",
    controller: UserController,
    action: "all"
}, {
    method: "get",
    route: "/users/:id",
    controller: UserController,
    action: "one"
}, {
    method: "post",
    route: "/users",
    controller: UserController,
    action: "save"
}, {
    method: "delete",
    route: "/users/:id",
    controller: UserController,
    action: "remove"
},
{
    method: "get",
    route: "/boothEvents",
    controller: BoothEventController,
    action: "all"
}, {
    method: "get",
    route: "/boothEvents/:id",
    controller: BoothEventController,
    action: "one"
}, {
    method: "post",
    route: "/boothEvents",
    controller: BoothEventController,
    action: "save"
}, {
    method: "delete",
    route: "/boothEvents/:id",
    controller: BoothEventController,
    action: "remove"
}]