import { Entity, PrimaryGeneratedColumn, Column } from "typeorm"

@Entity()
export class BoothEvent {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    messageType: string

    @Column()
    message: string

    @Column()
    json: string; // Json

    @Column()
    approved: boolean;

    @Column()
    received: Date;

}
