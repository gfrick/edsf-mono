import { getRepository } from "typeorm"
import { NextFunction, Request, Response } from "express"
import { User } from "../entity/User"
import { AppDataSource } from "../data-source";

export class UserController {


    async all(request: Request, response: Response, next: NextFunction) {
        let userRepository = AppDataSource.getRepository(User);
        return userRepository.find()
    }

    async one(request: Request, response: Response, next: NextFunction) {
        let userRepository = AppDataSource.getRepository(User);
        return userRepository.findOne(request.params.id)
    }

    async save(request: Request, response: Response, next: NextFunction) {
        let userRepository = AppDataSource.getRepository(User);
        return userRepository.save(request.body)
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let userRepository = AppDataSource.getRepository(User);
        let userToRemove = await userRepository.findOneBy({ id: request.params.id })
        await userRepository.remove(userToRemove)
    }

}