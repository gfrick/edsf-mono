import { NextFunction, Request, Response } from "express";
import { BoothEvent } from "../entity/BoothEvent";
import { AppDataSource } from "../data-source";
import {allKeys, allowedMessageTypes} from "../access";

export class BoothEventController {

    async all(request: Request, response: Response, next: NextFunction) {
        let boothEventRepository = AppDataSource.getRepository(BoothEvent);
        return boothEventRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        let boothEventRepository = AppDataSource.getRepository(BoothEvent);
        return boothEventRepository.findOne(request.params.id);
    }

    // todo, handle exception here and don't crash fcs
    async save(request: Request, response: Response, next: NextFunction) {
        const token = request.headers["authorization"];
        let event = request.body;

        const allowedTypes = allowedMessageTypes.find((amt) => amt.key === token).types;
        if( !allowedTypes.includes(event.messageType)) {
            return
        }
        console.log(allowedTypes);
        let boothEventRepository = AppDataSource.getRepository(BoothEvent);
        event.approved = "true";
        event.received = new Date();
        // TODO, a lot of stuff here like checking key and auto-tagging.
        return boothEventRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let boothEventRepository = AppDataSource.getRepository(BoothEvent);
        let boothEventToRemove = await boothEventRepository.findOneBy({ id: request.params.id });
        if( boothEventToRemove) {
            await boothEventRepository.remove(boothEventToRemove);
        }
    }

}
