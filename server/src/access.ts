
type UserKey = string;

const SkillsMatrixKey = "dd531564-d1a9-4146-8bfa-9c8c6d3fe606";
const MusicSoftware = "097fe657-d62d-49f6-82ca-d7ca0a3a4ba2";
const ThreeDWebsite = "4ebff047-8d3c-4e01-b30b-2c72752a9444";
const DesignSystem = "ccf09968-4c52-4474-bd01-348544e66f3a";
const VRWaveShooter = "c985eeac-af71-49eb-ac43-1e61efc9d0ca";
const MLSithVsJedi = "3a1d287e-7e57-464c-b0b5-e6595ece0b6b";
const LiverTransplant = "3afb50fd-219e-4123-a118-011a7e3c35cd";
const PoliceChase = "d496b52c-62f7-4a30-93e0-3fa8fc5d0a0f";
const Sprinternship = "b72416b2-5ab7-40cb-81b0-cb250ae08403";
const DeskSpace = "e68ee8f0-357b-47c9-9cf8-8cc086c288cd";
const Testing = "testing"; // Will be removed before conference.

const allKeys = [ SkillsMatrixKey, MusicSoftware, ThreeDWebsite, DesignSystem, VRWaveShooter,
 MLSithVsJedi, LiverTransplant, PoliceChase, Sprinternship, DeskSpace, Testing];

const allowedMessageTypes: {key: string, types: string[]}[] = [
    {key: SkillsMatrixKey, types: ["skillsmatrix"] },
    {key: MusicSoftware, types: ["musicsoftware"] },
    {key: ThreeDWebsite, types: ["threedwebsite"] },
    {key: DesignSystem, types: ["designsystem"] },
    {key: VRWaveShooter, types: ["vrwaveshooter"] },
    {key: MLSithVsJedi, types: ["sithjedi"] },
    {key: LiverTransplant, types: ["liver"] },
    {key: PoliceChase, types: ["policechase"] },
    {key: Sprinternship, types: ["sprinternship"] },
    {key: DeskSpace, types: ["deskspace"] },
    {key: Testing, types: ["testing", "edsf"] },
];

let ipMessageTypeBlock: {key: string, ip: string[], banned: boolean}[] = [
    {key: SkillsMatrixKey, ip: [], banned: false },
    {key: MusicSoftware, ip: [], banned: false },
    {key: ThreeDWebsite, ip: [], banned: false },
    {key: DesignSystem, ip: [], banned: false },
    {key: VRWaveShooter, ip: [], banned: false },
    {key: MLSithVsJedi, ip: [] , banned: false},
    {key: LiverTransplant, ip: [], banned: false },
    {key: PoliceChase, ip: [], banned: false },
    {key: Sprinternship, ip: [], banned: false },
    {key: DeskSpace, ip: [], banned: false },
    {key: Testing, ip: [] , banned: false},
];


export {
     allKeys, allowedMessageTypes, ipMessageTypeBlock
}