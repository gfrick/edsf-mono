import {DynamicBodyDirective} from "../app/dynamic-body/dynamic-body.directive";
import {Type} from "@angular/core";
import {DefaultBodyComponent} from "../app/dynamic-body/default-body/default-body.component";
import {PoiceChaseBoothEvent} from "./PoliceChaseEvent";

interface BoothEvent {
  id: number;
  messageType: string;
  message: string;
  json?: string; // Json
  approved: string;
  received: Date;
}

interface BoothEventPost extends BoothEvent {
  getAuthor(): string;

  getTagList(): string[];

  getImageUrl(): string;

  getBodyComponent(): Type<DynamicEventBody>
}

interface DynamicEventBody {

}

class DefaultBoothEventPost implements BoothEventPost {
  constructor(boothEvent: BoothEvent) {
    Object.assign(this, boothEvent);
  }

  approved = "";
  id = 0;
  message = "none";
  messageType = "";
  received = new Date();
  json?: string; // Json

  public getImageUrl(): string {
    return "assets/planet-core";
  }

  public getAuthor(): string {
    switch (this.messageType.toLowerCase()) {
      case "skillsmatrix":
        return "Christopher";
      case "musicsoftware":
        return "Music Software";
      case "threedwebsite":
        return "Tyler";
      case "designsystem":
        return "Oliver Ramirez";
      case "sithjedi":
        return "Josh Armstrong";
      case "liver":
        return "Naveen";
      case "vrwaveshooter":
        return "Greg";
      case "sprinternship":
        return "Sprinternship";
      case "deskspace":
        return "Desk Space";
      case "testing":
        return "George";
      case "edsf":
        return "George";
    }
    return this.messageType;
  }

  public getTagList(): string[] {
    switch (this.messageType.toLowerCase()) {
      case "skillsmatrix":
        return ["Skills Matrix"];
      case "musicsoftware":
        return ["Music Software"];
      case "threedwebsite":
        return ["Tyler"];
      case "designsystem":
        return ["Oliver Ramirez"];
      case "sithjedi":
        return ["Josh Armstrong"];
      case "liver":
        return ["Naveen"];
      case "vrwaveshooter":
        return ["Greg"];
      case "sprinternship":
        return ["Sprinternship"];
      case "deskspace":
        return ["Desk Space"];
      case "testing":
        return ["George"];
      case "edsf":
        return ["Event Driven", "Science Fair", "Node", "Angular", "Digital Ocean"];
    }
    return ["TechShowcase"];
  }

  getBodyComponent(): Type<DynamicEventBody> {
    return DefaultBodyComponent;
  }

}

class BoothEventPostFactory {
  public generateBoothEventPost(boothEvent: BoothEvent): BoothEventPost {
    switch (boothEvent.messageType.toLowerCase()) {
      case "policechase":
        return new PoiceChaseBoothEvent(boothEvent);
      default:
        return new DefaultBoothEventPost(boothEvent);
    }
  }
}

export {BoothEvent, BoothEventPost, BoothEventPostFactory, DynamicEventBody}
