import {BoothEvent, BoothEventPost, DynamicEventBody} from "./booth-event";
import {Type} from "@angular/core";
import {PoliceChaseBodyComponent} from "../app/dynamic-body/police-chase-body/police-chase-body.component";

class PoiceChaseBoothEvent implements BoothEventPost {
  constructor(boothEvent: BoothEvent) {
    Object.assign(this, boothEvent);
  }

  approved = "";
  id = 0;
  message = "none";
  messageType = "";
  received = new Date();
  json?: string; // Json

  public getImageUrl(): string {
    return "assets/planet-core";
  }

  public getAuthor(): string {
    return "Justin Montgomery";
  }

  public getTagList(): string[] {
    return [ "Unity", "Game", "Simulator"]; // TODO, more from his data
  }

  getBodyComponent(): Type<DynamicEventBody> {
    return PoliceChaseBodyComponent;
  }

}

export { PoiceChaseBoothEvent}
