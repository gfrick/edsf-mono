import { Component, OnInit, Input } from '@angular/core';
import {BoothEventPost} from "../../shared/booth-event";

@Component({
  selector: 'app-post-heading',
  templateUrl: './post-heading.component.html',
  styleUrls: ['./post-heading.component.scss']
})
export class PostHeadingComponent implements OnInit {

  @Input() eventItem: BoothEventPost | undefined;

  public subHead: string | undefined = undefined;

  constructor() { }

  ngOnInit(): void {
  }

}
