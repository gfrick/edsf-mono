import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {BoothEvent, BoothEventPost} from "../../shared/booth-event";
import {DynamicBodyDirective} from "../dynamic-body/dynamic-body.directive";

@Component({
  selector: 'app-event-item',
  templateUrl: './event-item.component.html',
  styleUrls: ['./event-item.component.scss']
})
export class EventItemComponent implements OnInit {

  @Input() eventItem: BoothEventPost | undefined;

  public subHead: string | undefined = undefined;

  constructor() { }

  ngOnInit(): void {
  }

}
