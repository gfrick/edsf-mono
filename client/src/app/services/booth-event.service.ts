import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {BoothEvent} from "../../shared/booth-event";

@Injectable({
  providedIn: 'root'
})
export class BoothEventService {

  constructor(private httpClient: HttpClient) { }

  public getEvents(): Observable<BoothEvent[]> {
    return this.httpClient.get<BoothEvent[]>("/api/boothEvents",
      {
        headers: {
          'Authorization': 'testing'
        }
      });
  }
}
