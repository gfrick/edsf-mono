import { TestBed } from '@angular/core/testing';

import { BoothEventService } from './booth-event.service';

describe('BoothEventService', () => {
  let service: BoothEventService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BoothEventService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
