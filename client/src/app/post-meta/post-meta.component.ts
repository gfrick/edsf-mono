import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {BoothEventPost} from "../../shared/booth-event";

@Component({
  selector: 'app-post-meta',
  templateUrl: './post-meta.component.html',
  styleUrls: ['./post-meta.component.scss']
})
export class PostMetaComponent implements OnInit, OnChanges {

  @Input() eventItem: BoothEventPost | undefined;
  public author = "Unknown";
  public tagList = ["TechShowcase"];

  constructor() { }

  ngOnInit(): void {
    this.cacheFields();
  }

  ngOnChanges(): void {
    this.cacheFields();
  }

  public cacheFields(): void {
    if( this.eventItem) {
      this.author = this.eventItem.getAuthor();
      this.tagList = this.eventItem.getTagList();
    }
  }

}
