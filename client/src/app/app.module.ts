import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { EventFeedComponent } from './event-feed/event-feed.component';
import { HeaderComponent } from './header/header.component';
import { EventItemComponent } from './event-item/event-item.component';
import {HttpClientModule} from "@angular/common/http";
import {PostMetaComponent} from "./post-meta/post-meta.component";
import {PostImageComponent} from "./post-image/post-image.component";
import {PostHeadingComponent} from "./post-heading/post-heading.component";
import {PostBodyComponent} from "./post-body/post-body.component";
import { DefaultBodyComponent } from './dynamic-body/default-body/default-body.component';
import { DynamicBodyDirective } from './dynamic-body/dynamic-body.directive';
import { PoliceChaseBodyComponent } from './dynamic-body/police-chase-body/police-chase-body.component';
import { EventCounterComponent } from './dashboard/event-counter/event-counter.component';
import { PieChartComponent } from './dashboard/pie-chart/pie-chart.component';
import {NgxChartsModule} from "@swimlane/ngx-charts";


@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    EventFeedComponent,
    HeaderComponent,
    EventItemComponent,
    PostBodyComponent,
    PostHeadingComponent,
    PostImageComponent,
    PostMetaComponent,
    DefaultBodyComponent,
    DynamicBodyDirective,
    PoliceChaseBodyComponent,
    EventCounterComponent,
    PieChartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxChartsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
