import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PoliceChaseBodyComponent } from './police-chase-body.component';

describe('PoliceChaseBodyComponent', () => {
  let component: PoliceChaseBodyComponent;
  let fixture: ComponentFixture<PoliceChaseBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PoliceChaseBodyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PoliceChaseBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
