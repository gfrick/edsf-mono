import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appDynamicBody]'
})
export class DynamicBodyDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
