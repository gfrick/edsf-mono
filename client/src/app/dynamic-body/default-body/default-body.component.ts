import { Component, OnInit } from '@angular/core';
import {DynamicEventBody} from "../../../shared/booth-event";

@Component({
  selector: 'app-default-body',
  templateUrl: './default-body.component.html',
  styleUrls: ['./default-body.component.scss']
})
export class DefaultBodyComponent implements DynamicEventBody, OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
