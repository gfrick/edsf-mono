import { Component, OnInit, Input } from '@angular/core';
import { BoothEventPost} from "../../shared/booth-event";

@Component({
  selector: 'app-post-image',
  templateUrl: './post-image.component.html',
  styleUrls: ['./post-image.component.scss']
})
export class PostImageComponent implements OnInit {

  @Input() eventItem: BoothEventPost | undefined;

  public imageUrl = "assets/planet-core";

  ngOnInit(): void {
    this.cacheFields();
  }

  ngOnChanges(): void {
    this.cacheFields();
  }

  public cacheFields(): void {
    if( this.eventItem) {
      this.imageUrl = this.eventItem.getImageUrl();
    }
  }
}
