import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {BoothEventPost, DynamicEventBody} from "../../shared/booth-event";
import {DynamicBodyDirective} from "../dynamic-body/dynamic-body.directive";

@Component({
  selector: 'app-post-body',
  templateUrl: './post-body.component.html',
  styleUrls: ['./post-body.component.scss']
})
export class PostBodyComponent implements OnInit {
  @ViewChild(DynamicBodyDirective, {static: true}) private dynamicHost!: DynamicBodyDirective;
  @Input() eventItem: BoothEventPost | undefined;

  constructor() {
  }

  ngOnInit(): void {
    this.cacheFields();
  }

  ngOnChanges(): void {
    this.cacheFields();
  }

  public cacheFields(): void {
    if (this.eventItem) {
      const viewContainerRef = this.dynamicHost.viewContainerRef;
      viewContainerRef.clear();
      viewContainerRef.createComponent<DynamicEventBody>(this.eventItem.getBodyComponent());
    }
  }

}
