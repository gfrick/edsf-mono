import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-counter',
  templateUrl: './event-counter.component.html',
  styleUrls: ['./event-counter.component.scss']
})
export class EventCounterComponent implements OnInit {

  public eventString = "000,000,001";
  constructor() { }

  ngOnInit(): void {
  }

}
