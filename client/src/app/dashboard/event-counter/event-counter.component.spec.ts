import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventCounterComponent } from './event-counter.component';

describe('EventCounterComponent', () => {
  let component: EventCounterComponent;
  let fixture: ComponentFixture<EventCounterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventCounterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EventCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
