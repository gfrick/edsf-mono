import { Component, OnInit } from '@angular/core';

export var single = [
  {
    "name": "Germany",
    "value": 8940000
  },
  {
    "name": "USA",
    "value": 5000000
  },
  {
    "name": "France",
    "value": 7200000
  },
  {
    "name": "UK",
    "value": 6200000
  }
];

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent {
   single = [
     {
       "name": "JediSith",
       "value": 10
     },
     {
       "name": "EDSF",
       "value": 5
     },
     {
       "name": "Police Chase",
       "value": 12
     },
     {
       "name": "VR Shooter",
       "value": 15
     }
   ];
  view: [number, number] = [400, 300];

  // options
  gradient: boolean = true;
  showLegend: boolean = false;
  showLabels: boolean = true;
  isDoughnut: boolean = false;
  legendPosition: string = 'below';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  constructor() {
  //  Object.assign(this, { single });
  }


}
