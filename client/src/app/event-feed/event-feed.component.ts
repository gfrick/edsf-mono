import { Component, OnInit } from '@angular/core';
import {BoothEvent, BoothEventPost, BoothEventPostFactory} from "../../shared/booth-event";
import {BoothEventService} from "../services/booth-event.service";
import {interval, map, startWith, switchMap} from "rxjs";

@Component({
  selector: 'app-event-feed',
  templateUrl: './event-feed.component.html',
  styleUrls: ['./event-feed.component.scss']
})
export class EventFeedComponent implements OnInit {

  public events: BoothEventPost[] = [];
  public factory = new BoothEventPostFactory();

  constructor(private boothEventService: BoothEventService) { }

  ngOnInit(): void {
    /**
     * Ok.
     * 1. Only poll for newer events, and obviously if you get an afternoon off or something switch
     *    this to websocket.
     * 2. Append the polled events.
     * 3. Switch it to append the newer events on top.
     * 4. Then resume working on API.
     */
    interval(50000)
      .pipe(
        startWith(0),
        switchMap(() => this.boothEventService.getEvents().pipe(
          map((result) => { return result.map((eachEvent)=> {
            eachEvent.received = new Date(eachEvent.received);
            return this.factory.generateBoothEventPost(eachEvent);
          }).sort( (eventA, eventB) =>  +eventB.received - +eventA.received )})
        ))
      )
      .subscribe((result) =>{
        this.events = result;
      });
  }

  private CreateEvent(eachEvent: BoothEvent) : BoothEvent {
    return eachEvent;
  }
}
