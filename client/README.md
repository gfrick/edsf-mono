Event Driven Science Fair

This project is a small event streaming dashboard
for the nvisia technology fair in december. The client
will poll the server for events and display them.
The server will listen for events from the other booths
and and record them.
Events must be approved since this is such a simple
project anyone could likely figure out the URL the
day of and start sending naughty things. So there
will need to be an admin screen and api for approving
posts. We'll allow some auto-approve to try and keep
everything real time - mostly just approve for events
with pictures such as the Jedi V Sith booth.
